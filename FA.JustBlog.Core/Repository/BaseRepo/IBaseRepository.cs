﻿namespace FA.JustBlog.Core.Repository.BaseRepo
{
    /// <summary>
    /// Interface defines the basic operations
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBaseRepository<T> where T : class
    {
        /// <summary>
        /// Adds an entity to the repository
        /// </summary>
        /// <param name="entity"></param>
        void Add(T entity);

        /// <summary>
        /// Adds a collection of entities to the repository
        /// </summary>
        /// <param name="entities"></param>
        void AddRange(IEnumerable<T> entities);

        /// <summary>
        /// Finds an entity in the repository by ID and returns it
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T Find(int id);

        /// <summary>
        /// Updates an existing entity in the repository
        /// </summary>
        /// <param name="entity"></param>
        void Update(T entity);

        /// <summary>
        /// Deletes an existing entity from the repository
        /// </summary>
        /// <param name="entity"></param>
        void Delete(T entity);

        /// <summary>
        /// Deletes an entity with the specified ID from the repository
        /// </summary>
        /// <param name="id"></param>
        void Delete(int id);

        /// <summary>
        /// Retrieves all entities from the repository and returns them as a list
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> GetAll();
    }

}

