﻿using FA.JustBlog.Core.Data_Context;
using Microsoft.EntityFrameworkCore;

namespace FA.JustBlog.Core.Repository.BaseRepo
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected readonly JustBlogContext context;
        protected DbSet<T> dbSet;

        public BaseRepository(JustBlogContext context = null)
        {
            this.context = context ?? new JustBlogContext();
            dbSet = context.Set<T>();
        }

        /// <summary>
        /// Adds a single entity to the database
        /// </summary>
        /// <param name="entity"></param>
        public void Add(T entity)
        {
            dbSet.Add(entity);
        }

        /// <summary>
        /// Adds a range of entities to the database
        /// </summary>
        /// <param name="entities"></param>
        public void AddRange(IEnumerable<T> entities)
        {
            dbSet.AddRange(entities);
        }

        /// <summary>
        /// Deletes a single entity from the database
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(T entity)
        {
            dbSet.Remove(entity);
        }

        /// <summary>
        /// Deletes an entity from the database based on its id
        /// </summary>
        /// <param name="id"></param>
        public void Delete(int id)
        {
            T entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        /// <summary>
        /// Finds an entity in the database based on its id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T Find(int id)
        {
            return dbSet.Find(id);
        }

        /// <summary>
        /// Returns all entities from the database
        /// </summary>
        /// <returns></returns>
        //public IList<T> GetAll()
        //{
        //    return dbSet.ToList();
        //}

        /// <summary>
        /// Updates a single entity in the database
        /// </summary>
        /// <param name="entity"></param>
        public void Update(T entity)
        {
            dbSet.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// Updates a range of entities in the database
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateRange(T entity)
        {
            dbSet.AttachRange(entity);
            context.Entry(entity).State = EntityState.Modified;
        }

        IEnumerable<T> IBaseRepository<T>.GetAll()
        {
            return dbSet.ToList();
        }
    }
}
