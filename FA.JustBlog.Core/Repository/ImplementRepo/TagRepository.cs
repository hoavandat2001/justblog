﻿using FA.JustBlog.Core.Data_Context;
using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository.BaseRepo;
using FA.JustBlog.Core.Repository.IRepository;

namespace FA.JustBlog.Core.Repository.ImplementRepo
{
    public class TagRepository : BaseRepository<Tag>, ITagRepository
    {
        public TagRepository(JustBlogContext context) : base(context) { }

        /// <summary>
        /// Get a list of popular tags
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public IEnumerable<Tag> GetPopularTags(int size)
        {
            var result = from t in context.Tags
                         orderby t.Count descending
                         select t;
            return result.Take(size).ToList();
        }

        /// <summary>
        /// Retrieves a tag by its URL slug
        /// </summary>
        /// <param name="urlSlug"></param>
        /// <returns></returns>
        public Tag GetTagByUrlSlug(string urlSlug)
        {
            var result = from t in context.Tags
                         where t.UrlSlug.Equals(urlSlug)
                         select t;
            return result.FirstOrDefault();
        }
    }
}
