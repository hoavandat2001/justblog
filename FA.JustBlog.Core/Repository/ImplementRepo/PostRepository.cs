﻿using FA.JustBlog.Core.Data_Context;
using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository.BaseRepo;
using FA.JustBlog.Core.Repository.IRepository;
using Microsoft.EntityFrameworkCore;

namespace FA.JustBlog.Core.Repository.ImplementRepo
{
    public class PostRepository : BaseRepository<Post>, IPostRepository
    {
        /// <summary>
        /// Initializes the PostRepository with an instance of JustBlogContext
        /// </summary>
        /// <param name="context"></param>
        public PostRepository(JustBlogContext context) : base(context) { }

        /// <summary>
        /// Gets the count of posts associated with a specific category.
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public int CountPostsForCategory(string category)
        {
            var result = from p in context.Posts
                         join c in context.Categories
                         on p.CategoryId equals (c.Id)
                         where c.Name.Equals(category)
                         select p;
            return result.Count();
        }

        /// <summary>
        /// Gets the count of posts associated with a specific tag.
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public int CountPostsForTag(string tag)
        {
            var result = from t in context.Tags
                         join pt in context.PostTagMaps
                         on t.Id equals pt.TagId
                         where t.Name.ToLower().Equals(tag.ToLower())
                         select t;
            return result.Count();
        }

        /// <summary>
        /// Finds a post based on its year, month, and URL slug.
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="urlSlug"></param>
        /// <returns></returns>
        public Post FindPost(int year, int month, string urlSlug)
        {
            var result = from p in context.Posts
                         where p.PostedOn.Year == year
                         where p.PostedOn.Month == month
                         where p.UrlSlug == urlSlug
                         select p;
            return result.FirstOrDefault();
        }

        /// <summary>
        /// Gets a list of the highest-rated posts based on rating.
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public IEnumerable<Post> GetHighestPosts(int size)
        {
            var result = from p in context.Posts
                         orderby p.Rate descending
                         select p;
            return result.Take(size).ToList();
        }

        /// <summary>
        /// Gets a list of the most recent posts based on posting date.
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public IEnumerable<Post> GetLatestPost(int size)
        {
            var result = from p in context.Posts
                         orderby p.PostedOn descending
                         select p;
            return result.Take(size).ToList();
        }

        /// <summary>
        /// Gets a list of the most viewed posts based on view count.
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public IEnumerable<Post> GetMostViewedPost(int size)
        {
            var result = from p in context.Posts
                         orderby p.ViewCount descending
                         select p;
            return result.Take(size).ToList();
        }

        /// <summary>
        /// Gets a list of all posts associated with a specific category.
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public IEnumerable<Post> GetPostsByCategory(string category)
        {
            var result = from p in context.Posts
                         join c in context.Categories
                         on p.CategoryId equals c.Id
                         where c.Name == category
                         select p;
            return result.ToList();
        }

        /// <summary>
        /// Gets a list of all posts published in a specific month and year.
        /// </summary>
        /// <param name="monthYear"></param>
        /// <returns></returns>
        public IEnumerable<Post> GetPostsByMonth(DateTime monthYear)
        {
            var result = from p in context.Posts
                         where p.PostedOn.Month == monthYear.Month
                         select p;
            return result.ToList();
        }

        /// <summary>
        /// Gets a list of all posts associated with a specific tag.
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public IEnumerable<Post> GetPostsByTag(string tag)
        {
            var result = from p in context.Posts
                         join pt in context.PostTagMaps on p.Id equals pt.PostId
                         join t in context.Tags on pt.TagId equals t.Id
                         where t.Name.ToLower().Equals(tag.ToLower())
                         select p;
            return result.ToList();
        }

        /// <summary>
        /// Get all posts with its categories and tags
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Post> GetPostsWithCategoryAndTags()
        {
            return context.Posts.Include(p => p.Category).Include(p => p.PostTags).ThenInclude(pt => pt.Tags).Include(p => p.Comments).ToList();
        }

        /// <summary>
        /// Gets a list of all posts that have been published.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Post> GetPublisedPosts()
        {
            var result = from p in context.Posts
                         where p.Published.Equals(true)
                         select p;
            return result.ToList();
        }

        /// <summary>
        /// Gets a list of all posts that have not been published.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Post> GetUnpublisedPosts()
        {
            var result = from p in context.Posts
                         where p.Published.Equals(false)
                         select p;
            return result.ToList();
        }

    }
}
