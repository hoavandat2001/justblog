﻿using FA.JustBlog.Core.Data_Context;
using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository.BaseRepo;
using FA.JustBlog.Core.Repository.IRepository;

namespace FA.JustBlog.Core.Repository.ImplementRepo
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(JustBlogContext context) : base(context) { }
    }
}
