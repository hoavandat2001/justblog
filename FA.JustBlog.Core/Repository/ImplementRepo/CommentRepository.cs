﻿using FA.JustBlog.Core.Data_Context;
using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository.BaseRepo;
using FA.JustBlog.Core.Repository.IRepository;

namespace FA.JustBlog.Core.Repository.ImplementRepo
{
    public class CommentRepository : BaseRepository<Comment>, ICommentRepository
    {
        public CommentRepository(JustBlogContext context) : base(context) { }

        /// <summary>
        /// Add a new comment with specific properties
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="commentName"></param>
        /// <param name="commentEmail"></param>
        /// <param name="commentTitle"></param>
        /// <param name="commentBody"></param>
        public void AddComment(int postId, string commentName, string commentEmail, string commentTitle, string commentBody)
        {
            // Create a new Comment object with the given properties
            Comment comment = new Comment
            {
                PostId = postId,
                Name = commentName,
                CommentHeader = commentTitle,
                CommentText = commentBody,
                CommentTime = DateTime.Now
            };

            // Add the comment to the context
            context.Comments.Add(comment);
        }

        /// <summary>
        /// Returns all comments for a given post ID
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        public IList<Comment> GetCommentsForPost(int postId)
        {
            var result = from c in context.Comments
                         where c.PostId == postId
                         select c;
            return result.ToList();
        }

        /// <summary>
        /// Returns all comments for a specific post object
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public IEnumerable<Comment> GetCommentsForPost(Post post)
        {
            var result = from c in context.Comments
                         where c.PostId == post.Id
                         select c;
            return result.ToList();
        }
    }
}
