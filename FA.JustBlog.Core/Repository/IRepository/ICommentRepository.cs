﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository.BaseRepo;

namespace FA.JustBlog.Core.Repository.IRepository
{
    public interface ICommentRepository : IBaseRepository<Comment>
    {
        /// <summary>
        /// Adds a new comment to the database for the specified post ID, with the provided name, email, title, and body.
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="commentName"></param>
        /// <param name="commentEmail"></param>
        /// <param name="commentTitle"></param>
        /// <param name="commentBody"></param>
        void AddComment(int postId, string commentName, string commentEmail, string commentTitle, string commentBody);

        /// <summary>
        /// Retrieves all comments associated with the specified post ID.
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        IList<Comment> GetCommentsForPost(int postId);

        /// <summary>
        /// Retrieves all comments associated with the specified Post object.
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        IEnumerable<Comment> GetCommentsForPost(Post post);

    }
}
