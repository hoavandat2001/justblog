﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository.BaseRepo;

namespace FA.JustBlog.Core.Repository.IRepository
{
    public interface IPostRepository : IBaseRepository<Post>
    {
        /// <summary>
        /// Find a post with specific year, month of post day and its url slug
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="urlSlug"></param>
        /// <returns></returns>
        Post FindPost(int year, int month, string urlSlug);

        /// <summary>
        /// Get posts that are published
        /// </summary>
        /// <returns></returns>
        IEnumerable<Post> GetPublisedPosts();

        /// <summary>
        /// Get posts that are unpublished
        /// </summary>
        /// <returns></returns>
        IEnumerable<Post> GetUnpublisedPosts();

        /// <summary>
        /// Get a list of latest posts on the blog.
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        IEnumerable<Post> GetLatestPost(int size);

        /// <summary>
        /// Get a list of post by month
        /// </summary>
        /// <param name="monthYear"></param>
        /// <returns></returns>
        IEnumerable<Post> GetPostsByMonth(DateTime monthYear);

        /// <summary>
        /// Count the number of post that belongs to a given category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        int CountPostsForCategory(string category);

        /// <summary>
        /// Get the list post that belongs to a given category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        IEnumerable<Post> GetPostsByCategory(string category);

        /// <summary>
        /// Count the number of post that belongs to a given tag
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        int CountPostsForTag(string tag);

        /// <summary>
        /// Get the list post that belongs to a given tag
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        IEnumerable<Post> GetPostsByTag(string tag);

        /// <summary>
        /// Get the list of post that has the most views
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        IEnumerable<Post> GetMostViewedPost(int size);

        /// <summary>
        /// Get the list of post that has the highest rate
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        IEnumerable<Post> GetHighestPosts(int size);

        /// <summary>
        /// Get all posts with its categories and tags
        /// </summary>
        /// <returns></returns>
        IEnumerable<Post> GetPostsWithCategoryAndTags();
    }
}
