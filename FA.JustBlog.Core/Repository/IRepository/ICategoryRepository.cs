﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository.BaseRepo;

namespace FA.JustBlog.Core.Repository.IRepository
{
    public interface ICategoryRepository : IBaseRepository<Category>
    {

    }
}
