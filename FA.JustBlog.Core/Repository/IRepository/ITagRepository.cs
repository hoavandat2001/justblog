﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository.BaseRepo;

namespace FA.JustBlog.Core.Repository.IRepository
{
    public interface ITagRepository : IBaseRepository<Tag>
    {
        /// <summary>
        /// Get a tag by a given url slug
        /// </summary>
        /// <param name="urlSlug"></param>
        /// <returns></returns>
        Tag GetTagByUrlSlug(string urlSlug);

        /// <summary>
        /// Get a list of popular tags
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        IEnumerable<Tag> GetPopularTags(int size);
    }
}
