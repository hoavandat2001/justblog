﻿using FA.JustBlog.Core.Repository.IRepository;

namespace FA.JustBlog.Core.Repository.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        ICategoryRepository _category { get; }
        ITagRepository _tag { get; }
        IPostRepository _post { get; }
        ICommentRepository _comment { get; }

        void SaveChange();
    }
}
