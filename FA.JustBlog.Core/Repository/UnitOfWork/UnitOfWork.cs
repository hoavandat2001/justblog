﻿using FA.JustBlog.Core.Data_Context;
using FA.JustBlog.Core.Repository.ImplementRepo;
using FA.JustBlog.Core.Repository.IRepository;

namespace FA.JustBlog.Core.Repository.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly JustBlogContext context;
        private ICategoryRepository categoryRepository;
        private ITagRepository tagRepository;
        private IPostRepository postRepository;
        private ICommentRepository commentRepository;

        public UnitOfWork(JustBlogContext context = null)
        {
            this.context = context ?? new JustBlogContext();
        }

        public ICategoryRepository _category => categoryRepository ?? new CategoryRepository(context);

        public ITagRepository _tag => tagRepository ?? new TagRepository(context);

        public IPostRepository _post => postRepository ?? new PostRepository(context);

        public ICommentRepository _comment => commentRepository ?? new CommentRepository(context);

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void SaveChange()
        {
            context.SaveChanges();
        }
    }
}
