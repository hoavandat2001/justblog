﻿using FA.JustBlog.Core.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FA.JustBlog.Core.Data_Context
{
    public class JustBlogContext : IdentityDbContext<AppUser>
    {

        public JustBlogContext()
        {
        }

        public JustBlogContext(DbContextOptions<JustBlogContext> options) : base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<PostTagMap> PostTagMaps { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<IdentityRole> IdentityRole { get; set; }



        public DbSet<AppUser> UsingIdentityUser { get; set; }



        public DbSet<IdentityUserRole<string>> IdentityUserRole { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=.;Database=JuslBlogDB;Trusted_Connection=True");
               
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(Category).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(Post).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(Tag).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(PostTagMap).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(Comment).Assembly);

            modelBuilder.Seed();

        }
    }
}
