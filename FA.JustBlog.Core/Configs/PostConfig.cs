﻿using FA.JustBlog.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FA.JustBlog.Core.Configs
{
    public class PostConfig : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.ToTable("Post");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Title).IsRequired();
            builder.Property(x => x.ShortDescription).HasColumnName("Short Description").HasMaxLength(255).IsRequired();
            builder.Property(x => x.PostContent).HasColumnName("Post Content").IsRequired();
            builder.Property(x => x.UrlSlug).HasMaxLength(255).IsRequired();
            builder.Property(x => x.Published).IsRequired();
            builder.Property(x => x.PostedOn).HasColumnName("Posted On").IsRequired();
            builder.Property(x => x.Modified).IsRequired();
            builder.Property(x => x.ViewCount).IsRequired();
            builder.Property(x => x.RateCount).IsRequired();
            builder.Property(x => x.TotalRate).IsRequired();
            builder.Property(x => x.Rate)
                .HasComputedColumnSql("TotalRate * 1.0 / NULLIF(RateCount, 0)")
                .HasPrecision(18, 2)
                .IsRequired();
            builder.HasOne(x => x.Category).WithMany(x => x.Posts).HasForeignKey(x => x.CategoryId);
            builder.HasMany(x => x.PostTags).WithOne(x => x.Posts).HasForeignKey(x => x.PostId);
        }
    }
}
