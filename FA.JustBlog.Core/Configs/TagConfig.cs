﻿using FA.JustBlog.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FA.JustBlog.Core.Configs
{
    public class TagConfig : IEntityTypeConfiguration<Tag>
    {
        public void Configure(EntityTypeBuilder<Tag> builder)
        {
            builder.ToTable("Tag");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.UrlSlug).HasMaxLength(255).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(1024).IsRequired();
            builder.Property(x => x.Count).IsRequired();
            builder.HasMany(x => x.PostTags).WithOne(x => x.Tags).HasForeignKey(x => x.TagId);
        }
    }
}
