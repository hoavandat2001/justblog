﻿using FA.JustBlog.UI.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using FA.JustBlog.UI.Models;
using FA.JustBlog.UI.Areas.Admin.Models;

namespace FA.JustBlog.UI.Data;

public class FAJustBlogUIContext : IdentityDbContext<FAJustBlogUIUser>
{
    public FAJustBlogUIContext(DbContextOptions<FAJustBlogUIContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        // Customize the ASP.NET Identity model and override the defaults if needed.
        // For example, you can rename the ASP.NET Identity table names and more.
        // Add your customizations after calling base.OnModelCreating(builder);
    }

    public DbSet<FA.JustBlog.UI.Areas.Admin.Models.PostModels>? PostModels { get; set; }

    public DbSet<FA.JustBlog.UI.Models.TagsModels>? TagsModels { get; set; }

 

}

