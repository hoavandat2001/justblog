﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository.UnitOfWork;
using FA.JustBlog.UI.Areas.Admin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace FA.JustBlog.UI.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = ("Blog Owner, Contributor"))]
    public class CategoryController : Controller
    {
        private IUnitOfWork uow;
        const int ITEMS_PER_PAGE = 3;
        public CategoryController(IUnitOfWork uow)
        {
            this.uow = uow;
        }
        public IActionResult Index([Bind(Prefix = "page")] int pageNumber)
        {
            if (pageNumber == 0)
                pageNumber = 1;
            var category = uow._category.GetAll();
            var totalItems = category.Count();
            int totalPages = (int)Math.Ceiling((double)totalItems / ITEMS_PER_PAGE);
            if (pageNumber > totalPages)
                return RedirectToAction(nameof(CategoryController.Index), new { page = totalPages });
            var categorys = category
            .Skip(ITEMS_PER_PAGE * (pageNumber - 1))
            .Take(ITEMS_PER_PAGE)
            .ToList();
            ViewData["pageNumber"] = pageNumber;
            ViewData["totalPages"] = totalPages;
            return View(categorys.AsEnumerable());
        }
        [Authorize(Roles = ("Blog Owner, Contributor"))]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Create
        [Authorize(Roles = ("Blog Owner, Contributor"))]
        [HttpPost]
        [Route("")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoryModels category)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    uow._category.Add(new Category() { Name = category.Name, UrlSlug = category.UrlSlug, Description = category.Description });
                    uow.SaveChange();
                    return RedirectToAction(nameof(Index));
                }
                
            }
            catch
            {
                return View();
            }
            return View(category);
        }
        [Authorize(Roles = ("Blog Owner, Contributor"))]
        public ActionResult Edit(int? id)
        {
            if (id.HasValue)
            {
                var category = uow._category.Find(id.Value);
                if (category != null)
                {
                    return View(category);
                }
            }
            return RedirectToAction(nameof(Index));
        }



        // POST: CategoriesController/Edit/5
        [Authorize(Roles = ("Blog Owner, Contributor"))]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int? id, CategoryModels category)
        {
            if (id != category.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                try
                {
                    if (id.HasValue)
                    {
                        uow._category.Update(new Category() { Id = category.Id, Name = category.Name, UrlSlug = category.UrlSlug, Description = category.Description });
                        uow.SaveChange();
                    }
                }
                catch (InvalidOperationException ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
                return RedirectToAction(nameof(Index));
            }
            return View(category);
        }
        // GET: Test/Delete/5
        [Authorize(Roles = ("Blog Owner"))]
        public ActionResult Delete(int? id)
        {
            if(id.HasValue)
            {
                uow._category.Delete(id.Value);
                uow.SaveChange();
            }
            return RedirectToAction(nameof(Index));
        }
        //Get: Detail
        public ActionResult Detail(int? id)
        {
            if (id.HasValue)
            {
                var category = uow._category.Find(id.Value);
                if (category != null)
                {
                    return View(category);
                }
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
