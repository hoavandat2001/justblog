﻿
using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository.UnitOfWork;
using FA.JustBlog.UI.Areas.Admin.Models;
using FA.JustBlog.UI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Razor.Language;
using System.Data;
using System.Xml.Linq;

namespace FA.JustBlog.UI.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = ("Blog Owner, Contributor"))]
    public class TagController : Controller
    {
        private IUnitOfWork uow;
        const int ITEMS_PER_PAGE = 5;
        public TagController(IUnitOfWork uow)
        {
            this.uow = uow;
        }
        public IActionResult Index([Bind(Prefix = "page")] int pageNumber)
        {
            if (pageNumber == 0)
                pageNumber = 1;
            var tag = uow._tag.GetAll();
            var totalItems = tag.Count();
            int totalPages = (int)Math.Ceiling((double)totalItems / ITEMS_PER_PAGE);
            if (pageNumber > totalPages)
                return RedirectToAction(nameof(CommentController.Index), new { page = totalPages });
            var categorys = tag
            .Skip(ITEMS_PER_PAGE * (pageNumber - 1))
            .Take(ITEMS_PER_PAGE)
            .ToList();
            ViewData["pageNumber"] = pageNumber;
            ViewData["totalPages"] = totalPages;
            return View(categorys.AsEnumerable());
        }
        [Authorize(Roles = ("Blog Owner, Contributor"))]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Create
        [HttpPost]
        [Authorize(Roles = ("Blog Owner, Contributor"))]
        public ActionResult Create(TagsModels tag)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    uow._tag.Add(new Tag() {Name = tag.Name, UrlSlug = tag.UrlSlug, Description = tag.Description, Count = tag.Count});
                    uow.SaveChange();
                    return RedirectToAction(nameof(Index));
                }

            }
            catch
            {
                return View();
            }
            return View(tag);
        }

        // GET: Test/Delete/5
        [Authorize(Roles = ("Blog Owner"))]
        public ActionResult Delete(int? id)
        {
            if (id.HasValue)
            {
                uow._tag.Delete(id.Value);
                uow.SaveChange();
            }
            return RedirectToAction(nameof(Index));
        }
        //Get: Detail
        public ActionResult Detail(int? id)
        {
            if (id.HasValue)
            {
                var tag = uow._tag.Find(id.Value);
                if (tag != null)
                {
                    return View(tag);
                }
            }
            return RedirectToAction(nameof(Index));
        }
        [Authorize(Roles = ("Blog Owner, Contributor"))]
        public ActionResult Edit(int? id)
        {
            if (id.HasValue)
            {
                var tags = uow._tag.Find(id.Value);
                if (tags != null)
                {
                    return View(tags);
                }
            }
            return RedirectToAction(nameof(Index));
        }
        // POST: CategoriesController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = ("Blog Owner, Contributor"))]
        public IActionResult Edit(int? id, TagsModels tags)
        {
            if (id != tags.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                try
                {
                    if (id.HasValue)
                    {
                        uow._tag.Update(new Tag() {Id = tags.Id, Name = tags.Name, Description = tags.Description, UrlSlug = tags.UrlSlug, Count = tags.Count, });
                        uow.SaveChange();
                    }
                }
                catch (InvalidOperationException ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tags);
        }
    }

}
