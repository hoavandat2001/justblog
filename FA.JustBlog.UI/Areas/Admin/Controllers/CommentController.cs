﻿
using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository.UnitOfWork;
using FA.JustBlog.UI.Areas.Admin.Models;
using FA.JustBlog.UI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace FA.JustBlog.UI.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = ("Blog Owner, Contributor"))]
    public class CommentController : Controller
    {
        
        private IUnitOfWork uow;
        const int ITEMS_PER_PAGE = 3;

        public CommentController(IUnitOfWork uow)
        {
            this.uow = uow;
        }
        public IActionResult Index([Bind(Prefix = "page")] int pageNumber)
        {

            if (pageNumber == 0)
                pageNumber = 1;
            var commenet = uow._comment.GetAll();
            var totalItems = commenet.Count();
            int totalPages = (int)Math.Ceiling((double)totalItems / ITEMS_PER_PAGE);
            if (pageNumber > totalPages)
                return RedirectToAction(nameof(CommentController.Index), new { page = totalPages });
            var categorys = commenet
            .Skip(ITEMS_PER_PAGE * (pageNumber - 1))
            .Take(ITEMS_PER_PAGE)
            .ToList();
            ViewData["pageNumber"] = pageNumber;
            ViewData["totalPages"] = totalPages;
            return View(categorys.AsEnumerable());
        }

        // GET: Test/Details/5
        [Authorize(Roles = ("Blog Owner"))]
        public ActionResult Delete(int? id)
        {
            if (id.HasValue)
            {
                uow._comment.Delete(id.Value);
                uow.SaveChange();
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: Test/Create
        [Authorize(Roles = ("Blog Owner, Contributor"))]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Test/Create
        [HttpPost]
        [Authorize(Roles = ("Blog Owner, Contributor"))]
        public ActionResult Create(CommentModel comment)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    uow._comment.Add(new Comment() { Name = comment.Name, Email = comment.Email, CommentHeader = comment.CommentHeader, CommentText = comment.CommentText, CommentTime = comment.CommentTime,PostId= comment.PostId });
                    uow.SaveChange();
                    return RedirectToAction(nameof(Index));
                }
                
            }
            catch
            {
                return View();
            }
            return View(comment);
        }

        // GET: Test/Edit/5
        [Authorize(Roles = ("Blog Owner, Contributor"))]
        public ActionResult Edit(int? id)
        {
            if (id.HasValue)
            {
                var comment = uow._comment.Find(id.Value);
                if (comment != null)
                {
                    return View(comment);
                }
            }
            return RedirectToAction(nameof(Index));
        }



        // POST: CategoriesController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = ("Blog Owner, Contributor"))]
        public IActionResult Edit(int? id, CommentModel comment)
        {
            if (id != comment.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                try
                {
                    if (id.HasValue)
                    {
                        uow._comment.Update(new Comment() {Id =comment.Id, Name = comment.Name, Email = comment.Email, CommentHeader = comment.CommentHeader, CommentText = comment.CommentText, CommentTime = comment.CommentTime, PostId = comment.PostId });
                        uow.SaveChange();
                    }
                }
                catch (InvalidOperationException ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
                return RedirectToAction(nameof(Index));
            }
            return View(comment);
        }

        // GET: Test/Delete/5
        public ActionResult Detail(int? id)
        {
            if (id.HasValue)
            {
                var comment = uow._comment.Find(id.Value);
                if (comment != null)
                {
                    return View(comment);
                }
            }
            return RedirectToAction(nameof(Index));
        }

    }
}
