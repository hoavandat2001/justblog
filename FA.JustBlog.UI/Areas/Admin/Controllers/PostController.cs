﻿
using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository.UnitOfWork;
using FA.JustBlog.UI.Areas.Admin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Xml.Linq;

namespace FA.JustBlog.UI.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = ("Blog Owner, Contributor"))]
    public class PostController : Controller
    {
        private IUnitOfWork uow;
        const int ITEMS_PER_PAGE = 3;
        public PostController(IUnitOfWork uow)
        {
            this.uow = uow;
        }
        public IActionResult Index([Bind(Prefix = "page")] int pageNumber)
        {
            if (pageNumber == 0)
                pageNumber = 1;
            var post = uow._post.GetAll();
            var totalItems = post.Count();
            int totalPages = (int)Math.Ceiling((double)totalItems / ITEMS_PER_PAGE);
            if (pageNumber > totalPages)
                return RedirectToAction(nameof(CommentController.Index), new { page = totalPages });
            var categorys = post
            .Skip(ITEMS_PER_PAGE * (pageNumber - 1))
            .Take(ITEMS_PER_PAGE)
            .ToList();
            ViewData["pageNumber"] = pageNumber;
            ViewData["totalPages"] = totalPages;
            return View(categorys.AsEnumerable());
        }
        public IActionResult GetlatesdPost()
        {
            var getlatedpost = uow._post.GetLatestPost(5);
            return View(getlatedpost.ToList());
        }
        public IActionResult GetMostPost()
        {
            var getmostpost = uow._post.GetMostViewedPost(5);
            return View(getmostpost.ToList());
        }
        [Authorize(Roles = ("Blog Owner"))]
        public IActionResult PublishedPosts()
        {
            var publishedpost = uow._post.GetPublisedPosts();
            return View(publishedpost.ToList());
        }
        [Authorize(Roles = ("Blog Owner"))]
        public IActionResult UnPublisherPost()
        {
            var unpublisherpost = uow._post.GetUnpublisedPosts();
            return View(unpublisherpost.ToList());
        }
        // GET: Test/Details/5
        public ActionResult PostDetails(int? id)
        {
            if (id.HasValue)
            {
                var postDetails = uow._post.Find(id.Value);
                if (postDetails != null)
                {
                    return View(postDetails);
                }
            }
           
            return RedirectToAction(nameof(Index));
        }

        // GET: Test/Create
        [Authorize(Roles = ("Blog Owner, Contributor"))]
        public ActionResult Create()
        {
            var category = uow._category.GetAll();
            ViewData["categories"] = new MultiSelectList(category, "Id", "Name");
            return View();
        }

        // POST: Test/Create
        [Authorize(Roles = ("Blog Owner, Contributor"))]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PostModels post)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    uow._post.Add(new Post() {Title = post.Title, ShortDescription = post.ShortDescription, PostContent = post.PostContent, UrlSlug = post.UrlSlug, Published = post.Published, PostedOn = post.PostedOn, Modified = post.Modified,ViewCount = post.ViewCount,RateCount = post.RateCount,TotalRate = post.TotalRate, CategoryId =  post.CategoryId});
                    uow.SaveChange();
                    return Redirect("/Admin/Post/Index");
                }
            }
            catch
            {
                return View();
            }
            return View(post);
        }

        // GET: Test/Edit/5
        //[Authorize(Roles = ("Blog Owner, Contributor"))]
        public ActionResult Edit(int? id)
        {
            var category = uow._category.GetAll();
            ViewData["categories"] = new MultiSelectList(category, "Id", "Name");
            if (id.HasValue)
            {
                var postEdit = uow._post.Find(id.Value);
                if (postEdit != null)
                {
                    return View(postEdit);
                }
            }
            return Redirect("/Admin/Post/Index");
        }

        // POST: Test/Edit/5
        //[Authorize(Roles = ("Blog Owner, Contributor"))]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int? id, PostModels post)
        {

            if (id != post.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                try
                {
                    if (id.HasValue)
                    {
                        uow._post.Update(new Post() {Id = post.Id, Title = post.Title, ShortDescription = post.ShortDescription, PostContent = post.PostContent, UrlSlug = post.UrlSlug, Published = post.Published, PostedOn = post.PostedOn, Modified = post.Modified, ViewCount = post.ViewCount, RateCount = post.RateCount, TotalRate = post.TotalRate, CategoryId = post.CategoryId });
                        uow.SaveChange();
                    }
                }
                catch (InvalidOperationException ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
                return Redirect("/Admin/Post/Index");
            }
            return View(post);
        }
        // POST: Test/Delete/5
        [Authorize(Roles = ("Blog Owner"))]
        public ActionResult Delete(int? id)
        {
            if (id.HasValue)
            {
                uow._post.Delete(id.Value);
                uow.SaveChange();
            }
            return Redirect("/Admin/Post/Index");
        }
    }
}
