﻿
using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repository.ImplementRepo;
using FA.JustBlog.Core.Repository.IRepository;
using FA.JustBlog.Core.Repository.UnitOfWork;
using FA.JustBlog.UI.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.UI.Controllers
{
    [Route("Post/{action}")]
    public class PostController : Controller
    {
        private IUnitOfWork uow;
        private readonly UserManager<FAJustBlogUIUser> _userManager;

        public PostController(IUnitOfWork uow, UserManager<FAJustBlogUIUser> _userManager)
        {
            this.uow = uow;
            this._userManager = _userManager;
        }

        [Route("")]
        public IActionResult Index()
        {
            var result = uow._post.GetAll();
            ViewBag.GetMostViewed = uow._post.GetMostViewedPost(5);
            ViewBag.GetLatestPost = uow._post.GetLatestPost(5);
            ViewBag.GetTags = uow._tag.GetAll();
            return View(result.ToList());
        }

        public IActionResult Detail(int? id)
        {
            if (id.HasValue)
            {
                var result = uow._post.Find(id.Value);
                var cmt = uow._comment.GetCommentsForPost(id.Value);
                if (result != null)
                {
                    result.Comments = cmt;
                    return View(result);
                }
            }
            return RedirectToAction("Index");
        }


        public IActionResult GetTags(string tag)
        {
            ViewBag.GetMostViewed = uow._post.GetMostViewedPost(5);
            ViewBag.GetLatestPost = uow._post.GetLatestPost(5);
            ViewBag.GetTags = uow._tag.GetAll();
            var tagPost = uow._post.GetPostsByTag(tag);
            if (tagPost != null)
            {
                return View(tagPost.ToList());
            }
            return RedirectToAction("Index");
        }
        public IActionResult GetPostByCategory(string category)
        {
            ViewBag.GetMostViewed = uow._post.GetMostViewedPost(5);
            ViewBag.GetLatestPost = uow._post.GetLatestPost(5);
            ViewBag.GetTags = uow._tag.GetAll();
            var catePost = uow._post.GetPostsByCategory(category);
            if (catePost != null)
            {
                return View(catePost.ToList());
            }
            return RedirectToAction("Index");
        }



        public async Task<IActionResult> CommentPost(int id, Comment comment)
        {
            var user = await _userManager.GetUserAsync(User);
            var post = uow._post.Find(id);
            if (User.Identity?.IsAuthenticated ?? false)
            {
                var postcmt = new Comment
                {
                    Name = user.FirstName + user.LastName,
                    CommentHeader = comment.CommentHeader,
                    PostId = id,
                    CommentText = comment.CommentText,
                    Email = user.Email,
                    CommentTime = DateTime.Now
                };
                uow._comment.Add(postcmt);
                uow.SaveChange();
            }
            else
            {
                var postcmtuser = new Comment
                {
                    Name = comment.Name,
                    CommentHeader = comment.CommentHeader,
                    PostId = id,
                    CommentText = comment.CommentText,
                    Email = comment.Email,
                    CommentTime = DateTime.Now
                };
                uow._comment.Add(postcmtuser);
                uow.SaveChange();
            }

            return RedirectToAction("Detail", "Post", new { id = post.Id });


        }

    }
}