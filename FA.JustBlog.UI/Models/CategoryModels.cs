﻿namespace FA.JustBlog.UI.Areas.Admin.Models
{
    public class CategoryModels
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UrlSlug { get; set; }
        public string Description { get; set; }
    }
}
